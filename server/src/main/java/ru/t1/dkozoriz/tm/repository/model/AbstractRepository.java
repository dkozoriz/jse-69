package ru.t1.dkozoriz.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.dkozoriz.tm.model.AbstractModel;

@NoRepositoryBean
public interface AbstractRepository<T extends AbstractModel> extends JpaRepository<T, String> {

}
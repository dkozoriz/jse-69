package ru.t1.dkozoriz.tm.api.repository.model;

import ru.t1.dkozoriz.tm.model.business.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
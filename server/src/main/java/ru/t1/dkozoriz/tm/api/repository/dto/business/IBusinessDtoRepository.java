package ru.t1.dkozoriz.tm.api.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.BusinessModelDto;

import java.util.List;

public interface IBusinessDtoRepository<T extends BusinessModelDto> extends IUserOwnedDtoRepository<T> {

    @NotNull
    List<T> findAllOrderByName(@NotNull String userId);

    @NotNull
    List<T> findAllOrderByStatus(@NotNull String userId);

    @NotNull
    List<T> findAllOrderByCreated(@NotNull String userId);

}
package ru.t1.dkozoriz.tm.api.service.model;

import ru.t1.dkozoriz.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
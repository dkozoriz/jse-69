package ru.t1.dkozoriz.tm.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.dkozoriz.tm.dto.model.business.BusinessModelDto;
import ru.t1.dkozoriz.tm.repository.dto.UserOwnedDtoRepository;

import java.util.List;

@NoRepositoryBean
public interface BusinessDtoRepository<T extends BusinessModelDto> extends UserOwnedDtoRepository<T> {

    @NotNull
    List<T> findAllByUserId(@NotNull final String userId, @NotNull Sort sort);

}
package ru.t1.dkozoriz.tm.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends BusinessDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectIdAndUserId(@Nullable String userId, @NotNull String projectId);

}
package ru.t1.dkozoriz.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends JpaRepository<UserDto, String> {

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

}
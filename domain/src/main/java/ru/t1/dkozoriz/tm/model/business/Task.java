package ru.t1.dkozoriz.tm.model.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
public class Task extends BusinessModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}

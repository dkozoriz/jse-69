package ru.t1.dkozoriz.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class ListClearTaskResponse extends AbstractResponse {
}
package ru.t1.dkozoriz.tm.listener.system;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    public ApplicationAboutListener() {
        super("about", "show developer info.", "-a");
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + systemEndpoint.getAbout(new ServerAboutRequest()).getName());
        System.out.println("E-mail: " + systemEndpoint.getAbout(new ServerAboutRequest()).getEmail() + "\n");

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getApplicationName() + "\n");

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + systemEndpoint.getAbout(new ServerAboutRequest()).getBranch());
        System.out.println("COMMIT ID: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitId());
        System.out.println("COMMITTER NAME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitterName());
        System.out.println("COMMITTER EMAIL: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitterEmail());
        System.out.println("MESSAGE: " + systemEndpoint.getAbout(new ServerAboutRequest()).getMessage());
        System.out.println("TIME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getTime());
    }

}
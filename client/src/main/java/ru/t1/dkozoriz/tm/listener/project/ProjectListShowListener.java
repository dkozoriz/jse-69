package ru.t1.dkozoriz.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectShowListRequest;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListShowListener extends AbstractProjectListener {

    public ProjectListShowListener() {
        super("project-list", "show project list.");
    }

    @Override
    @EventListener(condition = "@projectListShowListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @Nullable final List<ProjectDto> projects =
                projectEndpoint.projectList(new ProjectShowListRequest(getToken(), sort)).getProjectList();
        int index = 1;
        for (final ProjectDto project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
package ru.t1.dkozoriz.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    Project findByUserIdAndId(final String userId, final String id);

    void deleteByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    List<Project> findAllByUserId(final String userId);

    long countByUserId(final String userId);

}
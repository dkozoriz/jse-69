package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.service.UserService;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserEndpoint {

    private final UserService userService;

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping("/getAll")
    public Set<User> getAll() {
        return new HashSet<>(userService.findAll());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping("/count")
    public Long count() {
        return userService.count();
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping("/get/{id}")
    public User get(
            @PathVariable("id") String id
    ) {
        return userService.findById(id);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PostMapping("/post")
    public User post(
            @RequestBody User user) {
        return userService.save(user);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping("/put")
    public User put(
            @RequestBody User user
    ) {
        return userService.update(user);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        userService.deleteById(id);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        userService.deleteAll();
    }

}
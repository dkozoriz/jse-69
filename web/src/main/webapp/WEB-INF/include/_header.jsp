<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <title>TASK MANAGER</title>
</head>
<style>

    h1 {
        font-size: 20px;
    }

    table {
        width: 100%;
    }

    td, th {
        border: 1px solid #ededed;
        text-align: left;
    }

    a.menu {
        text-decoration: none;
        margin-right:20px;
    }

</style>
<body>


    <a class="menu" href="/index">MAIN</a></li>
    <a class="menu" href="/projects">PROJECTS</a>
    <a class="menu" href="/tasks">TASKS</a>
    <sec:authorize access="!isAuthenticated()">
        <a class="menu" href="/login" >LOGIN</a>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <a class="menu" href="/logout" >LOGOUT</a>
    </sec:authorize>

<br/>
</body>